@php
$baseUrl = 'https://bob-humphrey.com';
@endphp
<div
  class="flex flex-col lg:flex-row lg:justify-end text-lg lg:text-xl font-nunito_light text-blue-900 leading-none pb-8 lg:pb-0">
  <a href="{{ $baseUrl }}/experience" class="hover:text-blue-500 self-center no-underline px-2 py-2">
    Experience
  </a>
  <a href="{{ $baseUrl }}/work" class="hover:text-blue-500 self-center no-underline px-2 py-2">
    Work
  </a>
  <a href="{{ $baseUrl }}/projects" class="hover:text-blue-500 self-center no-underline px-2 py-2">
    Projects
  </a>
  <a href="{{ $baseUrl }}/skills" class="hover:text-blue-500 self-center no-underline px-2 py-2">
    Skills
  </a>
  <a href="{{ $baseUrl }}/articles" class="hover:text-blue-500 self-center no-underline px-2 py-2">
    Articles
  </a>
  <a href="/" class="text-blue-500 self-center no-underline px-2 py-2">
    Contact
  </a>
</div>
