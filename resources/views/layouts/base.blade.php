<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="Bob Humphrey is a web developer with years of experience. This site includes examples of his recent work, a list of his skills, and a collection of articles he has written.">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  @bukStyles
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body>

  {{ $slot }}

  <!-- Scripts -->
  @livewireScripts
  @bukScripts
  <script src="{{ asset('js/all.js') }}" defer></script>

</body>

</html>
