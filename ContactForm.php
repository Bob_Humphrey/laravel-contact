<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Models\ContactFormEntry;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class ContactForm extends Component
{
  public const ADDITION = 1;
  public const SUBTRACTION = 2;
  public const MULTIPLICATION = 3;

  public $operation;
  public $operationSymbol;
  public $spamFilterFirstNumber;
  public $spamFilterSecondNumber;
  public $spamFilterExpectedResult;
  public $name;
  public $email;
  public $subject;
  public $message;
  public $spam_filter_result;
  public $ip_address;

  protected $rules = [
    'name' => 'required|string|min:2',
    'email' => 'required|email',
    'subject' => 'present',
    'message' => 'required|string|min:10',
    'spam_filter_result' => 'required|string|numeric',
  ];

  public function sendMessage()
  {
    $this->validate();

    $contactFormEntry = new ContactFormEntry();
    $contactFormEntry->name = $this->name;
    $contactFormEntry->email = $this->email;
    $contactFormEntry->subject = $this->subject;
    $contactFormEntry->message = $this->message;
    $contactFormEntry->spam_filter_result = $this->spam_filter_result;
    $contactFormEntry->spam_filter_expected_result = $this->spamFilterExpectedResult;
    $contactFormEntry->ip_address = $this->ip_address;

    // Reject messages with a subject
    if (strlen(trim($this->subject))) {
      $contactFormEntry->rejected_subject = true;
      $contactFormEntry->save();
      return redirect()->to('/');
    }

    // Reject messages with incorrect spam filter results
    if ($this->spam_filter_result != $this->spamFilterExpectedResult) {
      $contactFormEntry->rejected_result = true;
      $contactFormEntry->save();
      session()->flash('danger', 'Incorrect answer to the arithmetic problem. Are you really a person?');
      return redirect()->to('/');
    }

    // Mail::to($_ENV['MAIL_TO_ADDRESS'])
    Mail::to('rphumphrey@gmail.com')
      ->send(new ContactMail([
        'name' => $contactFormEntry->name,
        'email' => $contactFormEntry->email,
        'content' => $contactFormEntry->message
      ]));

    $contactFormEntry->save();
    session()->flash('success', 'Your message has been received. Thank you!');
    return redirect()->to('/');
  }

  public function mount(Request $request)
  {
    //Log::info($_ENV);
    $this->ip_address = $request->getClientIp();
    $this->createSpamFilterQuestion();
  }

  public function render()
  {
    return view('livewire.contact-form');
  }

  protected function createSpamFilterQuestion()
  {
    $this->operation = rand(1, 3);

    if ($this->operation === self::ADDITION) {
      $this->spamFilterFirstNumber = rand(2, 9);
      $this->spamFilterSecondNumber = rand(2, 9);
      $this->spamFilterExpectedResult = $this->spamFilterFirstNumber
        + $this->spamFilterSecondNumber;
      $this->operationSymbol = '+';
    } elseif ($this->operation === self::SUBTRACTION) {
      $this->spamFilterFirstNumber = rand(10, 19);
      $this->spamFilterSecondNumber = rand(1, 9);
      $this->spamFilterExpectedResult = $this->spamFilterFirstNumber
        - $this->spamFilterSecondNumber;
      $this->operationSymbol = '-';
    } elseif ($this->operation === self::MULTIPLICATION) {
      $this->spamFilterFirstNumber = rand(2, 6);
      $this->spamFilterSecondNumber = rand(3, 7);
      $this->spamFilterExpectedResult = $this->spamFilterFirstNumber
        * $this->spamFilterSecondNumber;
      $this->operationSymbol = 'X';
    }
  }
}
