<x-alert type="success" class="bg-green-700 text-white p-4 mb-4 mx-auto" />
<x-alert type="warning" class="bg-yellow-600 text-white p-4 mb-4 mx-auto" />
<x-alert type="danger" class="bg-red-700 text-white p-4 mb-4 mx-auto" />
