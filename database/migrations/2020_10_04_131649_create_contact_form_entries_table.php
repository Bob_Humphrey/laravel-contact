<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactFormEntriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('contact_form_entries', function (Blueprint $table) {
      $table->id();
      $table->string('name', 100);
      $table->string('email', 100);
      $table->string('subject', 100)->nullable(true);
      $table->text('message');
      $table->string('spam_filter_result', 2)->nullable(true);
      $table->string('spam_filter_expected_result', 2)->nullable(true);
      $table->string('ip_address', 30);
      $table->boolean('rejected_subject')->default(false);
      $table->boolean('rejected_result')->default(false);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('contact_form_entries');
  }
}
