<x-layouts.base>
  <div class="font-nunito_regular">
    <header class="bg-gray-50 py-2">
      <div class="flex flex-col lg:flex-row w-full lg:justify-between lg:px-10 xl:px-32 py-3">

        <div class="flex lg:justify-start pb-4 lg:pb-0">
          <a href="/" class="flex flex-col lg:flex-row w-full text-center lg:text-left">
            <h1 class="self-center font-nunito_bold text-3xl text-blue-900 no-underline">
              Bob Humphrey
            </h1>
            <div class="self-center font-nunito_light text-3xl text-blue-500 lg:pl-4">
              Web Developer
            </div>
          </a>
        </div>

        <div
          class="flex flex-col lg:flex-row lg:justify-end text-lg font-nunito_light lg:text-xl text-blue-900 leading-none pb-8 lg:pb-0">
          <a href="https://bob-humphrey.com/experience" class="hover:text-blue-500 self-center no-underline px-2 py-2">
            Experience
          </a>
          <a href="https://bob-humphrey.com/work" class="self-center no-underline px-2 py-2">
            Work
          </a>
          <a href="https://bob-humphrey.com/projects" class="hover:text-blue-500 self-center no-underline px-2 py-2">
            Projects
          </a>
          <a href="https://bob-humphrey.com/skills" class="hover:text-blue-500 self-center no-underline px-2 py-2">
            Skills
          </a>
          <a href="https://bob-humphrey.com/articles" class="hover:text-blue-500 self-center no-underline px-2 py-2">
            Articles
          </a>
          <a href="/" class="text-blue-500 self-center no-underline px-2 py-2">
            Contact
          </a>
        </div>
      </div>
    </header>
    <main class="p-6 w-full mx-auto">

      {{ $slot }}

    </main>

    @include('layouts.footer')
  </div>
</x-layouts.base>
